# vaccine-population-denominator

This project looks at how population denominator (e.g., Health Service Utilisation, Estimated Residential Population) alters vaccination coverage.

## Folders

### code

The `code` folder contains any code used for subsequent analysis of data, such as summary statistics or maps. 

The file `denominator_analysis.rmd` contains all the code to produce figures and tables comparing HSU and ERP denominators. It also creates an "adjusted" HSU denominator that makes the HSU population equal the number of partial (1 dose) vaccinations in cases where this number exceeds the population. 

The file `hybrid_denominator.r` is a work in progress script that looks at a method of creating a denominator that combines HSU with ERP. The goal is to consider the excess population for Maaori in ERP and give them the same vaccination rate as Maaori in HSU, and then take this population away from the Other HSU population. This may be a way to mitigate the issue with Maaori being classified as Pakeha (Other) in HSU. 

### inputs

The `inputs` folder contains any additional input files required for analysis of `data` files.

### outputs

The `outputs` folder will be the target folder for any results saved by `code` files.
